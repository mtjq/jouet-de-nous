# Jouet de nous (definitive name)

This is a small, basic game made with the 
[Bevy engine](https://bevyengine.org/) in two days by 
[Louis Dumont](https://gitlab.com/ldmnt) (dev, music) and 
[Martin Jocqueviel](https://gitlab.com/mtjq) (dev).

SFX sounds are from [Kenney](https://www.kenney.nl)'s free assets website.

![gameplay](resources/gameplay.png)
![title](resources/title.png)
