use bevy::{
  prelude::*,
  window::PresentMode,
};
use jouet_de_nous::GamePlugin;

fn main() {
  App::new()
    .add_plugins(
      DefaultPlugins.set(
        WindowPlugin {
          primary_window: Some(Window {
            title: "Jouet de nous".into(),
            present_mode: PresentMode::AutoNoVsync,
            ..default()
          }),
          ..default()
        })
    )
    .add_plugins(GamePlugin)
    .run();
}
