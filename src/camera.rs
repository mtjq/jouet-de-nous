use bevy::{
  prelude::*,
  render::camera::ScalingMode,
};

pub const SCREEN_SIZE: Vec2 = Vec2::new(1920., 1080.);

pub struct CameraPlugin;

impl Plugin for CameraPlugin {
  fn build(&self, app: &mut App) {
    app.add_systems(Startup, spawn_camera);
  }
}

fn spawn_camera(mut cmd: Commands) {
  cmd.spawn(
    Camera2dBundle {
      projection: OrthographicProjection {
        near: -1000.,
        far: 1000.,
        scaling_mode: ScalingMode::AutoMin {
          min_width: SCREEN_SIZE.x,
          min_height: SCREEN_SIZE.y,
        },
        ..default()
      },
      ..default()
    }
  );
}
