use bevy::prelude::*;
use leafwing_input_manager::prelude::*;

pub struct InputPlugin;

#[derive(Actionlike, PartialEq, Eq, Clone, Copy, Hash, Debug, Reflect)]
pub enum Action {
    Right,
    Left,
    Up,
    Down,
    ClockwiseTurn,
    CounterClockwiseTurn,
    HalfTurn,
    VerticalFlip,
    HorizontalFlip,
}

impl Plugin for InputPlugin {
  fn build(&self, app: &mut App) {
    app.add_plugins(InputManagerPlugin::<Action>::default());
  }
}
