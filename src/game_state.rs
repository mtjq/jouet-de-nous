use std::time::Duration;

use bevy::{
  prelude::*,
  time::Stopwatch,
};

use crate::piece;

const GAME_TITLE: &str = "Jouet de nous";
pub const BACKGROUND_COLOR: &str = "#252933";
const TEXT_COLOR: &str = "#eceff4";
const NORMAL_BUTTON_COLOR: &str = "#434c5e";

const PIECES_PER_PLAY: u32 = 5;

pub struct GameStatePlugin;

impl Plugin for GameStatePlugin {
  fn build(&self, app: &mut App) {
    app
      .add_state::<GameState>()
      .insert_resource(LastScore(None))
      .add_systems(OnEnter(GameState::Menu), menu_setup)
      .add_systems(OnExit(GameState::Menu), despawn_entities_with_tag::<InMenu>)
      .add_systems(OnEnter(GameState::Play), play_setup)
      .add_systems(OnExit(GameState::Play), despawn_entities_with_tag::<InPlay>)
      .add_systems(
        Update,
        menu_action.run_if(in_state(GameState::Menu)),
      )
      .add_systems(
        Update,
        check_victory
          .run_if(in_state(GameState::Play))
          .after(piece::check_goal_matched)
      )
      ;
  }
}

#[derive(Component)]
pub struct InMenu;

#[derive(Component, Default)]
pub struct InPlay;

#[derive(Clone, Copy, Default, Eq, PartialEq, Debug, Hash, States)]
pub enum GameState {
    #[default]
    Menu,
    Play,
}

#[derive(Copy, Clone, Debug)]
struct Score {
  time: Duration,
}

#[derive(Resource)]
struct LastScore(Option<Score>);

#[derive(Component)]
enum MenuButtonAction {
  Play,
}

fn menu_setup(mut cmd: Commands, last_score: Res<LastScore>) {
  let button_style = Style {
    width: Val::Px(500.0),
    height: Val::Px(65.0),
    margin: UiRect::all(Val::Px(20.0)),
    justify_content: JustifyContent::Center,
    align_items: AlignItems::Center,
    ..default()
  };

  let button_text_style = TextStyle {
      font_size: 40.0,
      color: Color::hex(TEXT_COLOR).unwrap_or_default(),
      ..default()
  };

  let title =
    TextBundle::from_section(
      GAME_TITLE,
      TextStyle {
        font_size: 80.0,
        color: Color::hex(TEXT_COLOR).unwrap_or_default(),
        ..default()
      },
    ).with_style(
      Style {
        margin: UiRect {
          top: Val::Px(200.0),
          bottom: Val::Px(50.0),
          ..default()
        },
        ..default()
      }
    );

  let root = (
    NodeBundle {
      style: Style {
        width: Val::Percent(100.0),
        align_items: AlignItems::Center,
        justify_content: JustifyContent::Center,
        ..default()
      },
      ..default()
    },
    InMenu,
  );

  let background =
    NodeBundle {
      style: Style {
        flex_direction: FlexDirection::Column,
        height: Val::Percent(100.),
        width: Val::Percent(100.),
        align_items: AlignItems::Center,
        ..default()
      },
      background_color:
        BackgroundColor(Color::hex(BACKGROUND_COLOR).unwrap_or_default()),
      ..default()
    };

  let play_button = (
    ButtonBundle {
      style: button_style.clone(),
      background_color:
        Color::hex(NORMAL_BUTTON_COLOR).unwrap_or_default().into(),
      ..default()
    },
    MenuButtonAction::Play,
  );

  let play_button_contents =
    TextBundle::from_section(
      "Press SPACE to play.",
      button_text_style.clone(),
    );

  let (last_score_visible, score_text) =
    match last_score.0 {
      Some(score) => {(
        Visibility::Visible,
        format!("Last time: {:.3}", score.time.as_secs_f32())
      )},
      None =>
        (Visibility::Hidden, "Woops, this shouldn't be visible. :3".into()),
    };
  let last_score =
    TextBundle {
      text:
        Text::from_section(
          score_text,
          TextStyle {
            font_size: 50.0,
            color: Color::hex(TEXT_COLOR).unwrap_or_default(),
            ..default()
          },
        ),
      visibility: last_score_visible,
      ..default()
    }.with_style(
      Style {
        margin: UiRect::all(Val::Px(100.0)),
        ..default()
      }
    );

  let instructions =
    TextBundle {
      text:
        Text::from_section(
          "Match the shapes.\nESDF to move, JL to rotate.",
          TextStyle {
            font_size: 50.0,
            color: Color::hex(TEXT_COLOR).unwrap_or_default(),
            ..default()
          },
        ),
      ..default()
    }.with_style(
      Style {
        margin: UiRect {
          top: Val::Px(100.0),
          bottom: Val::Px(50.0),
          ..default()
        },
        ..default()
      }
    ).with_text_alignment(TextAlignment::Center);

  cmd
    .spawn(root)
    .with_children(|parent| {
      parent
        .spawn(background)
        .with_children(|parent| {
          parent.spawn(title);
          parent
            .spawn(play_button)
            .with_children(|parent| {
              parent.spawn(play_button_contents);
            });
          parent.spawn(instructions);
          parent.spawn(last_score);
        });
    });
}

#[derive(Resource, Default)]
pub struct TimerScore {
  pub timer: Stopwatch,
}

fn play_setup(mut cmd: Commands) {
  cmd.insert_resource(PiecesDone::default());
  cmd.insert_resource(TimerScore::default());
  piece::spawn_pieces(cmd, None);
}

fn menu_action(
  input: Res<Input<KeyCode>>,
  mut state: ResMut<NextState<GameState>>,
) {
  if input.just_pressed(KeyCode::Space) {
    state.set(GameState::Play);
  }
}

#[derive(Resource, Default)]
struct PiecesDone {
  done: u32,
}

fn check_victory(
  mut cmd: Commands,
  to_despawn: Query<Entity, With<InPlay>>,
  player_piece: Query<&piece::Piece, With<piece::Player>>,
  mut events: EventReader<piece::GoalMatched>,
  mut state: ResMut<NextState<GameState>>,
  mut last_score: ResMut<LastScore>,
  mut pieces_done: ResMut<PiecesDone>,
  time_score: Res<TimerScore>,
) {
  if !events.is_empty() {
    events.clear();
    pieces_done.done += 1;
    if pieces_done.done == PIECES_PER_PLAY {
      last_score.0 = Some(Score {time: time_score.timer.elapsed()});
      state.set(GameState::Menu);
      return
    }
    let player_translation = player_piece.single().translation;
    for entity in &to_despawn {
      cmd.entity(entity).despawn_recursive();
    }
    piece::spawn_pieces(cmd, Some(player_translation));
  }
}

fn despawn_entities_with_tag<T: Component>(
  to_despawn: Query<Entity, With<T>>,
  mut cmd: Commands,
) {
  for entity in &to_despawn {
    cmd.entity(entity).despawn_recursive();
  }
}
