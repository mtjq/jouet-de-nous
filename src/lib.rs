use bevy::prelude::*;
use crate::camera::CameraPlugin;
use crate::input::InputPlugin;
use crate::piece::PiecePlugin;
use crate::game_state::{BACKGROUND_COLOR, GameStatePlugin};
use crate::audio::AudioPlugin;

mod camera;
pub mod piece;
mod input;
mod game_state;
mod audio;

pub struct GamePlugin;

impl Plugin for GamePlugin {
  fn build(&self, app: &mut App) {
    app
      .insert_resource(
        ClearColor(Color::hex(BACKGROUND_COLOR).unwrap_or_default())
      )
      .add_plugins(CameraPlugin)
      .add_plugins(InputPlugin)
      .add_plugins(PiecePlugin)
      .add_plugins(GameStatePlugin)
      .add_plugins(AudioPlugin)
      ;
  }
}
