use rand::random;
use bevy::prelude::*;
use leafwing_input_manager::prelude::*;

use crate::camera::SCREEN_SIZE;
use crate::input::Action;
use crate::game_state::{GameState, InPlay, TimerScore};
use crate::audio::{PlaySound, Sound};


const GOAL_COLOR: &str = "#6a7894";
const PLAYER_COLOR: &str = "#eceff4";
const GOAL_DISTANCE: i32 = 8;
pub const BLOCK_SIZE: f32 = 30.0;
pub const MIN_PIECE_SIZE: usize = 3;
pub const MAX_PIECE_SIZE: usize = 4;
const MIN_BLOCKS_PER_PIECE: usize = 2;

const LEVEL_MARGIN: i32 = 10;
const LEVEL_BOUND: IVec2 = IVec2::new(
  (SCREEN_SIZE.x / 2.0 / BLOCK_SIZE) as i32 - LEVEL_MARGIN,
  (SCREEN_SIZE.y / 2.0 / BLOCK_SIZE) as i32 - LEVEL_MARGIN
);

pub struct PiecePlugin;

impl Plugin for PiecePlugin {
  fn build(&self, app: &mut App) {
    app
      .add_systems(
        Update,
        (
          movement,
          update_world_translation.after(movement),
          update_blocks.after(update_world_translation),
          check_goal_matched.after(update_blocks),
        ).run_if(in_state(GameState::Play))
      )
      .add_event::<GoalMatched>()
      ;
  }
}

#[derive(Bundle, Default)]
pub struct PieceBundle {
  piece: Piece,
  spatial: SpatialBundle,
  matrix: Matrix,
  in_play: InPlay,
}

impl PieceBundle {
  fn new(translation: IVec2, matrix: Vec<Vec<u32>>) -> Self {
    PieceBundle {
      piece: Piece {translation},
      matrix: Matrix {matrix},
      spatial: SpatialBundle {
        transform: Transform {
          translation: Vec3::new(
            BLOCK_SIZE * translation.x as f32,
            BLOCK_SIZE * translation.y as f32,
            0.0
          ),
          ..default()
        },
        ..default()
      },
      ..default()
    }
  }
}

#[derive(Component, Default, Debug)]
pub struct Piece {
  pub translation: IVec2,
}

#[derive(Component, PartialEq, Eq, Debug)]
pub struct Matrix {
  matrix: Vec<Vec<u32>>,
}

impl Default for Matrix {
  fn default() -> Self {
    Matrix {
      matrix: vec!(
        vec!(0, 0, 0),
        vec!(1, 1, 1),
        vec!(0, 1, 0)
      ),
    }
  }
}

impl Matrix {
  pub fn random(size: usize) -> Vec<Vec<u32>> {
    let mut matrix = vec![
      vec![
        0;
        size
      ];
      size
    ];

    for i in 0..size {
      for j in 0..size {
        if random() {
          matrix[i][j] = 1;
        }
      }
    }
    matrix
  }

  pub fn set_matrix(&mut self, matrix: Vec<Vec<u32>>) {
    self.matrix = matrix;
  }

  pub fn rotate(&mut self, angle: i32) {
    self.matrix = rotate_matrix(&self.matrix, angle);
  }

  pub fn flip(&mut self, _axis: Axis) {
  }
}

fn rotate_matrix(current_matrix: &Vec<Vec<u32>>, angle: i32) -> Vec<Vec<u32>> {
  let mut new_matrix: Vec<Vec<u32>> = Vec::new();
  for i in 0..current_matrix.len() {
    new_matrix.push(Vec::new() as Vec<u32>);
    for j in 0..current_matrix[i].len() {
      if angle == 1 {
        new_matrix[i].push(
          current_matrix[j][(current_matrix[i].len() - 1) - i]
        );
      } else if angle == -1 {
        new_matrix[i].push(
          current_matrix[(current_matrix.len() - 1) - j][i]
        );
      }
    }
  }
  new_matrix
}

#[derive(Component, Default)]
pub struct Block;

#[derive(Bundle)]
pub struct BlockBundle {
  block: Block,
  sprite: SpriteBundle,
}

#[derive(Component, Debug)]
pub struct Player;

impl Player {
  fn get_input_map() -> InputMap<Action> {
    InputMap::new([
      (QwertyScanCode::F, Action::Right),
      (QwertyScanCode::S, Action::Left),
      (QwertyScanCode::E, Action::Up),
      (QwertyScanCode::D, Action::Down),
      (QwertyScanCode::L, Action::ClockwiseTurn),
      (QwertyScanCode::J, Action::CounterClockwiseTurn),
      (QwertyScanCode::I, Action::VerticalFlip),
      (QwertyScanCode::K, Action::HorizontalFlip),
    ])
  }
}

#[derive(Component, Debug)]
pub struct Goal;

#[derive(Component)]
pub struct JustSpawned;

#[derive(Copy, Clone, Debug)]
pub enum Axis {
  Horizontal,
  Vertical,
}

pub fn spawn_pieces(mut cmd: Commands, player_translation: Option<IVec2>) {
  let matrix = create_matrix();
  spawn_piece(&mut cmd, &matrix, true, player_translation);
  spawn_piece(&mut cmd, &matrix, false, player_translation);
}

fn create_matrix() -> Vec<Vec<u32>> {
  let mut matrix: Vec<Vec<u32>> = Vec::new();
  let mut is_valid = false;
  while !is_valid {
    let size = random::<f32>();
    let size = size * (MAX_PIECE_SIZE - MIN_PIECE_SIZE + 1) as f32;
    let size = size.floor() as usize + MIN_PIECE_SIZE;
    matrix = Matrix::random(size);
    if count_blocks(&matrix) < MIN_BLOCKS_PER_PIECE {
      continue;
    }
    is_valid = minimize_matrix_size(&mut matrix);
    if matrix.len() < MIN_PIECE_SIZE {
      is_valid = false;
    }
  }
  matrix
}

fn count_blocks(matrix: &Vec<Vec<u32>>) -> usize {
  let mut usize = 0;
  // TODO: use iterators
  for i in 0..matrix.len() {
    for j in 0..matrix.len() {
      if matrix[i][j] == 1 {
        usize += 1;
      }
    }
  }
  usize
}

fn minimize_matrix_size(matrix: &mut Vec<Vec<u32>>) -> bool {
  // [[x_min, x_max], [y_min, y_max]]
  let mut corners = [[matrix.len()-1, 0], [matrix[0].len()-1, 0]];
  for i in 0..matrix.len() {
    for j in 0..matrix[i].len() {
      if matrix[i][j] == 1 {
        corners[0][0] = std::cmp::min(corners[0][0], i);
        corners[0][1] = std::cmp::max(corners[0][1], i);
        corners[1][0] = std::cmp::min(corners[1][0], j);
        corners[1][1] = std::cmp::max(corners[1][1], j);
      }
    }
  }
  if corners[0][0] + matrix.len() - corners[0][1] - 1
      != corners[1][0] + matrix.len() - corners[1][1] - 1 {
    return false;
  }
  matrix.truncate(corners[0][1] + 1);
  *matrix = matrix.split_off(corners[0][0]);
  for i in 0..matrix.len() {
    matrix[i].truncate(corners[1][1] + 1);
    (*matrix)[i] = matrix[i].split_off(corners[1][0]);
  }
  return true;
}

fn spawn_piece(
  cmd: &mut Commands,
  matrix: &Vec<Vec<u32>>,
  is_player: bool,
  player_translation: Option<IVec2>
) {
  let z = if is_player {1.0} else {0.0};
  let mut pos_abs = player_translation.unwrap_or_default();
  if pos_abs.x.abs() > LEVEL_BOUND.x {
    pos_abs.x = pos_abs.x.signum() * LEVEL_BOUND.x;
  }
  if pos_abs.y.abs() > LEVEL_BOUND.y {
    pos_abs.y = pos_abs.y.signum() * LEVEL_BOUND.y;
  }
  let mut pos_rel = if is_player {
    IVec2::ZERO
  } else {
    let x = random::<f32>() * (GOAL_DISTANCE * 2 + 1) as f32;
    let x = x.floor() as i32 - GOAL_DISTANCE;
    let y = (GOAL_DISTANCE - x.abs()) * if random() {1} else {-1};
    IVec2::new(x, y)
  };
  if (pos_abs.x + pos_rel.x).abs() > LEVEL_BOUND.x {
    pos_rel.x *= -1;
  }
  if (pos_abs.y + pos_rel.y).abs() > LEVEL_BOUND.y {
    pos_rel.y *= -1;
  }
  let pos = pos_abs + pos_rel;

  let color = Color::hex(
    if is_player {
      PLAYER_COLOR
    } else {
      GOAL_COLOR
    }
  ).unwrap_or_default();
  let rotation = if random() {1} else {-1};
  let matrix = if is_player {
    rotate_matrix(&matrix, rotation)
  } else {
    matrix.clone()
  };
  let piece_id = cmd.spawn(
    PieceBundle::new(pos, matrix.clone())
  ).id();

  if is_player {
    cmd.entity(piece_id).insert(Player);
    cmd.entity(piece_id).insert(JustSpawned);
    cmd.entity(piece_id).insert(InputManagerBundle::<Action> {
      action_state: ActionState::default(),
      input_map: Player::get_input_map(),
    });
  } else {
    cmd.entity(piece_id).insert(Goal);
  }

  for i in 0..matrix.len() {
    for j in 0..matrix[i].len() {
      if matrix[i][j] == 1 {
        let block_id = cmd.spawn(
          BlockBundle {
            block: Block,
            sprite: SpriteBundle {
              sprite: Sprite {
                color,
                custom_size: Some(Vec2::new(BLOCK_SIZE, BLOCK_SIZE)),
                ..default()
              },
              transform: Transform {
                translation: Vec3::new(30.0 * i as f32, 30.0 * j as f32, z),
                ..default()
              },
              ..default()
            },
          },
        ).id();
        if is_player {
          cmd.entity(block_id).insert(Player);
        } else {
          cmd.entity(block_id).insert(Goal);
        }

        cmd.entity(piece_id).add_child(block_id);
      }
    }
  }
}

fn movement(
  mut cmd: Commands,
  mut query: Query<
    (
      Entity,
      &mut Piece,
      &mut Matrix,
      &ActionState<Action>,
      Option<&JustSpawned>
    ),
    With<Player>
  >,
  mut event: EventWriter<PlaySound>,
) {
  for (entity, mut piece, mut matrix, input, just_spawned) in query.iter_mut() {
    let mut moved = false;

    if just_spawned.is_some() {
      cmd.entity(entity).remove::<JustSpawned>();
      return
    }

    {
      let mut dir = IVec2::ZERO;
      if input.just_pressed(Action::Right) { dir.x += 1 }
      if input.just_pressed(Action::Left) { dir.x -= 1 }
      if input.just_pressed(Action::Up) { dir.y += 1 }
      if input.just_pressed(Action::Down) { dir.y -= 1 }

      if dir != IVec2::ZERO {
        piece.translation += dir;
        moved = true;
      }
    }

    {
      let mut angle = 0;
      if input.just_pressed(Action::CounterClockwiseTurn) { angle += 1 }
      if input.just_pressed(Action::ClockwiseTurn) { angle -= 1 }

      if angle != 0 {
        matrix.rotate(angle);
        moved = true;
      }
    }

    {
      if input.just_pressed(Action::HorizontalFlip) {
        matrix.flip(Axis::Horizontal);
        moved = true;
      }

      if input.just_pressed(Action::VerticalFlip) {
        matrix.flip(Axis::Vertical);
        moved = true;
      }

    }
    if moved {
      event.send(PlaySound(Sound::Move));
    }
  }
}

fn update_world_translation(
  mut query: Query<(&mut Transform, &Piece), With<Player>>
) {
  for (mut transform, piece) in query.iter_mut() {
    transform.translation.x = piece.translation.x as f32 * BLOCK_SIZE;
    transform.translation.y = piece.translation.y as f32 * BLOCK_SIZE;
  }
}

fn update_blocks(
  piece_query: Query<&Matrix, (With<Player>, With<Piece>)>,
  mut blocks_query: Query<&mut Transform, (With<Player>, With<Block>)>
) {
  let Matrix{matrix} = piece_query.single();
  let mut block_transform_iter = blocks_query.iter_mut();

  for i in 0..matrix.len() {
    for j in 0..matrix[i].len() {
      if matrix[i][j] == 1 {
        block_transform_iter.next().unwrap_or_else(
          || {panic!("Not enough blocks in the piece to match its matrix.");}
        ).translation = Vec3::new(
          BLOCK_SIZE * i as f32,
          BLOCK_SIZE * j as f32,
          1.0
        );
      }
    }
  }
}

#[derive(Event)]
pub struct GoalMatched;

pub fn check_goal_matched(
  player_query: Query<(&Transform, &Matrix), With<Player>>,
  goal_query: Query<(&Transform, &Matrix), With<Goal>>,
  mut event: EventWriter<GoalMatched>,
  mut goal_sound: EventWriter<PlaySound>,
  time: Res<Time>,
  mut time_score: ResMut<TimerScore>,
) {
  time_score.timer.tick(time.delta());
  let (player_transform, player_matrix) = player_query.single();
  let (goal_transform, goal_matrix) = goal_query.single();
  if
    player_transform.translation == goal_transform.translation
    && player_matrix == goal_matrix
  {
    event.send(GoalMatched);
    goal_sound.send(PlaySound(Sound::Match));
  }
}
