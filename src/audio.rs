use bevy::prelude::*;
use bevy::audio::{
  Volume,
  VolumeLevel,
  PlaybackSettings, PlaybackMode
};

pub struct AudioPlugin;

impl Plugin for AudioPlugin {
  fn build(&self, app: &mut App) {
    app
      .add_event::<PlayMusic>()
      .add_event::<PlaySound>()
      .add_systems(Startup, startup)
      .add_systems(Update, on_play_music)
      .add_systems(Update, on_play_sound)
      ;
  }
}

#[derive(Component)]
struct Music;

fn startup(mut cmd: Commands, asset_server: Res<AssetServer>) {
  cmd.spawn((
    AudioBundle {
      source: asset_server.load("audio/main_theme.ogg"),
      settings: PlaybackSettings {
        mode: PlaybackMode::Loop,
        ..default()
      },
      ..default()
    },
    Music,
  ));
}

#[derive(Event)]
pub struct PlayMusic {pub on: bool}

#[derive(Event)]
pub struct PlaySound(pub Sound);

pub enum Sound {
  Move,
  Match,
}

fn on_play_music(
  mut events: EventReader<PlayMusic>,
  music_controller: Query<&AudioSink, With<Music>>
) {
  for event in events.iter() {
    if let Ok(sink) = music_controller.get_single() {
      if event.on { sink.play() } else { sink.stop() }
    }
  }
}

fn on_play_sound(
  mut cmd: Commands,
  mut events: EventReader<PlaySound>,
  asset_server: Res<AssetServer>,
) {
  for event in events.iter() {
    match event.0 {
      Sound::Move => {
        cmd.spawn(AudioBundle {
          source: asset_server.load(
            "audio/move.ogg"
          ),
          settings: PlaybackSettings {
            mode: PlaybackMode::Despawn,
            volume: Volume::Relative(VolumeLevel::new(0.7)),
            ..default()
          }
        });
      }
      Sound::Match => {
        cmd.spawn(AudioBundle {
          source: asset_server.load("audio/match.ogg"),
          settings: PlaybackSettings {
            mode: PlaybackMode::Despawn,
            volume: Volume::Relative(VolumeLevel::new(0.4)),
            ..default()
          }
        });
      }
    }
  }
}
